# Base class for deployment services
#
# These services integrate with a deployment solution like Kubernetes/OpenShift,
# Mesosphere, etc, to provide additional features to environments.
class TestService < DeploymentService
  include Gitlab::CurrentSettings

  def self.supported_events
    %w()
  end

  def predefined_variables
    []
  end

  def title
    'Test'
  end

  def description
    'A test'
  end

  def self.to_param
    'test'
  end
  # Environments may have a number of terminals. Should return an array of
  # hashes describing them, e.g.:
  #
  #     [{
  #       :selectors    => {"a" => "b", "foo" => "bar"},
  #       :url          => "wss://external.example.com/exec",
  #       :headers      => {"Authorization" => "Token xxx"},
  #       :subprotocols => ["foo"],
  #       :ca_pem       => "----BEGIN CERTIFICATE...", # optional
  #       :created_at   => Time.now.utc
  #     }]
  #
  # Selectors should be a set of values that uniquely identify a particular
  # terminal
  def terminals(environment)
    return [{
	:url => "ws://localhost:2375/v1.24/containers/31a1fec34542/attach/ws?logs=1&stderr=1&stdout=1&stream=1&stdin=1"
    }]
    #raise NotImplementedError
  end
end
